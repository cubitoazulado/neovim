" _________    ___    ___ ___      ___ ___  _____ ______      
"|\___   ___\ |\  \  /  /|\  \    /  /|\  \|\   _ \  _   \    
"\|___ \  \_| \ \  \/  / | \  \  /  / | \  \ \  \\\__\ \  \   
"     \ \  \   \ \    / / \ \  \/  / / \ \  \ \  \\|__| \  \  
"      \ \  \   \/  /  /   \ \    / /   \ \  \ \  \    \ \  \ 
"       \ \__\__/  / /      \ \__/ /     \ \__\ \__\    \ \__\
"        \|__|\___/ /        \|__|/       \|__|\|__|     \|__|
"            \|___|/                                          
"                                                             
syntax on
syntax enable
set autoindent
set nocompatible 
set mouse=a
set showcmd
set showmatch
set noerrorbells
set sw=2
set expandtab
set smartindent
set number
set rnu
set numberwidth=1
set nowrap
set noswapfile
set nobackup
set incsearch
set ignorecase
set clipboard=unnamed
set encoding=UTF-8
set cursorline
set laststatus=2
set noshowmode
set termguicolors

highlight ColoColumn ctermbg=0 guibg=#0D82CF

call plug#begin("~/.local/share/nvim/plugged")
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'lifepillar/pgsql.vim'
Plug 'ap/vim-css-color'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'ryanoasis/vim-devicons'
"Plug 'https://github.com/tc50cal/vim-terminal' " Vim Terminal
Plug 'preservim/tagbar'
Plug 'terryma/vim-multiple-cursors'
Plug 'easymotion/vim-easymotion'
Plug 'christoomey/vim-tmux-navigator'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'omnisharp/omnisharp-vim'
Plug 'yggdroot/indentline'
Plug 'maximbaz/lightline-ale'
Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline-themes'
"Telescope"
Plug 'kyazdani42/nvim-web-devicons'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
"Close pairs () [] {} ''
"Cerrar los pares () [] {} '' 
Plug 'jiangmiao/auto-pairs'
Plug 'alvan/vim-closetag'
Plug 'romgrk/barbar.nvim'
Plug 'mhinz/vim-startify'
Plug 'mattn/emmet-vim'
Plug 'othree/html5.vim'
Plug 'sheerun/vim-polyglot'
Plug 'ervandew/supertab'
Plug 'sirver/ultisnips'
Plug 'honza/vim-snippets'
call plug#end()

"KeyMaster
let mapleader = " "
map <C-f> :Files<CR>
map <leader>b :Buffers<CR>
nnoremap <leader>rg :Rg<CR>
nnoremap <leader>tg :Tags<CR>
nnoremap <leader>mk :Marks<CR>
nnoremap <leader>bl :BLines<CR>
nnoremap <leader>h  :History<CR>

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <Leader>t :te<CR>
"Modificar tamaño de ventana"
nmap <c-right> :vertical resize +5<CR>
nmap <c-left> :vertical resize -5<CR>
nmap <c-up> :resize +5<CR>
nmap <c-down> :resize -5<CR>
"Abrir ventana"
nmap <Leader>a :tabe<CR>
"Navegar entre ventanas"
nmap <Leader>k :bnext<CR>
nmap <Leader>j :bprevious<CR>
"Ventana Vertical - Horizontal"
nmap <Leader>vs :vsp<CR>
nmap <Leader>sp :sp<CR>
"Abrir html en navegador"
nnoremap <Leader>. :silent update<Bar>silent !brave %:p &<CR>

"TAGBAR
nmap <leader>bt :TagbarToggle<CR>
"telescope
nnoremap <leader>tf <cmd>Telescope find_files<cr>
nnoremap <leader>tl <cmd>Telescope live_grep<cr>
nnoremap <leader>tb <cmd>Telescope buffers<cr>
nnoremap <leader>th <cmd>Telescope help_tags<cr>

nnoremap <leader>ts <cmd>Telescope git_status<cr>
nnoremap <leader>ch <cmd>Telescope command_history<cr>

nmap <Leader>s <Plug>(easymotion-s2)
"Explorador de archivos
nmap <Leader>nn :NERDTreeFind<CR>

"Navegar entre pestañas barbar"
nnoremap <leader>1 :BufferGoto 1<CR>
nnoremap <leader>2 :BufferGoto 2<CR>
nnoremap <leader>3 :BufferGoto 3<CR>
nnoremap <leader>4 :BufferGoto 4<CR>
nnoremap <leader>5 :BufferGoto 5<CR>
nnoremap <leader>6 :BufferGoto 6<CR>
nnoremap <leader>7 :BufferGoto 7<CR>
nnoremap <leader>8 :BufferGoto 8<CR>
nnoremap <leader>9 :BufferLast<CR>

" Close buffer
nnoremap <silent>qq :BufferClose<CR>

"Navegar entre pestañas default"
"noremap <leader>1 1gt"
"noremap <leader>2 2gt"
"noremap <leader>3 3gt"
"noremap <leader>4 4gt"
"noremap <leader>5 5gt"
"noremap <leader>6 6gt"
"noremap <leader>7 7gt"
"noremap <leader>8 8gt"
"noremap <leader>9 9gt"
noremap <leader>0 :tablast<cr>"

nnoremap <C-z> :tabprevious<CR>
nnoremap <Tab> :tabnext<CR>

" Coc

nmap <silent> cd <Plug>(coc-definition)
nmap <silent> cy <Plug>(coc-type-definition)
nmap <silent> ci <Plug>(coc-implementation)
nmap <silent> cr <Plug>(coc-references)

" .. EMMET -- "
let g:user_emmet_mode='n'
let g:user_emmet_leader_key=','
"Seleccionar Color-Theme
set background="dark" 
colorscheme purify
let g:lightline = { 
      \ 'colorscheme': 'onedark',
      \ }
let g:airline_theme = "deus"

" HTML, JSX 
let g:closetag_filenames = '*.html,*.js,*.jsx,*.ts,*.tsx'

"Lightline -Ale "

let g:lightline#ale#indicator_checking = "\uf110"
let g:lightline#ale#indicator_infos = "\uf129" 
let g:lightline#ale#indicator_warnings = "\uf071"
let g:lightline#ale#indicator_errors = "\uf05e"
let g:lightline#ale#indicator_ok = "\uf00c"

let g:lightline = {
       \ 'active': {
      \   'left': [['mode', 'paste'], [], ['relativepath', 'modified']],
      \   'right': [['kitestatus'], ['filetype', 'percent', 'lineinfo'], ['gitbranch']]
      \ },
      \ 'inactive': {
      \   'left': [['inactive'], ['relativepath']],
      \   'right': [['bufnum']]
      \ },
      \ 'component': {
      \   'bufnum': '%n',
      \   'inactive': 'inactive'
      \ },
      \}
let g:lightline.component_expand = {
      \  'linter_checking': 'lightline#ale#checking',
      \  'linter_infos': 'lightline#ale#infos',
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors',
      \  'linter_ok': 'lightline#ale#ok',
      \ }

let g:lightline.active = { 'right': [[ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ]] }


" --------------Configuracion FZF -------------- "
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

let g:fzf_tags_command = 'ctags -R'
" Border color
let g:fzf_layout = {'up':'~90%', 'window': { 'width': 0.8, 'height': 0.8,'yoffset':0.5,'xoffset': 0.5, 'highlight': 'Todo', 'border': 'sharp' } }
let $FZF_DEFAULT_OPTS = '--layout=reverse --info=inline'
"Get Files
command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(<q-args>, fzf#vim#with_preview({'options': ['--layout=reverse', '--info=inline']}), <bang>0)

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
  \   fzf#vim#with_preview(), <bang>0)

function! RipgrepFzf(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction

command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)

let g:python3_host_prog = '/usr/bin/python3'


